from ultralyticsplus import YOLO, render_result


# use trained model to predict
model = YOLO('runs/detect/train/weights/best.pt')
image = 'datasets/detect/test/images/yorkshire_terrier_174_jpg.rf.9276193d1ed03940e2e376a3d31b5361.jpg'
results = model.predict(image, imgsz=640)

# how to use predicted
result = results[0]

# [top left x, top left y, width, height]
boxes = result.boxes.xyxy
scores = result.boxes.conf
categories = result.boxes.cls

# save image to local optional
render = render_result(model=model, image=image, result=result)
render.save('result.jpg', 'JPEG')