## 简介
这个仓库分享了使用ultralytics框架来训练CV模型的故事。训练了三种模型，图片分类、目标检测、语义分割。标注平台，[roboflow](roboflow.com)支持yolo8导出数据的是图片分类和目标检测，[labelstud](https://labelstud.io/)支持yolo8导出数据的是语义分割。在Docker中进行调试，保留了数据集和模型结果（包括评估信息和模型文件），分类看\play-model\runs\classify\train\results.png评估结果，top1精确度慢慢向上增加，损失慢慢降低，代表模型可用。目标检测和语义分割看，mAP这个指标，方法一样。详细的介绍在docs目录中的ppt里。这个仓库比较大（900MB），可以单独下载需要的数据或模型。


## 如何玩
```commandline
docker run -itd --name utrlts1 --shm-size 8G -v /home/code/play-model:/workspace/play-model --gpus all ultralytics/ultralytics
docker exec -it utrlts bash
cd /workspace/play-model
python det-train.py
python det-predict.py
```
