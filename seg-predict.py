from ultralyticsplus import YOLO, render_result


# use trained model to predict
model = YOLO('runs/segment/train/weights/best.pt')
image = 'datasets/segment/test/images/000000000086.jpg'
results = model.predict(image, imgsz=640)

# how to use predicted
result = results[0]

# save image to local optional
render = render_result(model=model, image=image, result=result)
render.save('result.jpg', 'JPEG')
