from ultralytics import YOLO


# use trained model to predict
model = YOLO('runs/classify/train/weights/best.pt')
image = 'datasets/classify/test/cat/Cat_99_99_jpeg.rf.005b7bde3e5bfe58e9f5233e785dae8b.jpg
results = model.predict(image, imgsz=640)

# how to use predicted
result = results[0]

# confidence
con = result.probs
# classification
cls = result.names