from ultralytics import YOLO


# use labeled data and detect model to train
model = YOLO('yolov8s.pt')
model.train(data='/workspace/play-model/datasets/detect/data.yaml', epochs=56, device='1,3')