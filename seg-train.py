from ultralytics import YOLO


# use labeled data and detect model to train
model = YOLO('yolov8x-seg.pt')
# batch sizes evenly divisible by len(devices)
model.train(data='/workspace/play-model/datasets/segment/data.yaml', epochs=1024, device='1,2,3', batch=15)
