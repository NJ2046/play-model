from ultralytics import YOLO


# use labeled data and detect model to train
model = YOLO('yolov8s-cls.pt')
# batch sizes evenly divisible by len(devices)
model.train(data='/workspace/play-model/datasets/classify', epochs=128, device='3')